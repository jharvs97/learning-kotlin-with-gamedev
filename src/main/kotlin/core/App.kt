package core

import game.Game
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.coroutines.Runnable
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.withContext
import org.khronos.webgl.WebGLRenderingContext as GL
import org.w3c.dom.HTMLCanvasElement
import math.*
import kotlin.coroutines.EmptyCoroutineContext

class App(private val width: Int = 960, private val height: Int = 540)  {
    private var lastGameTime: Double = 0.0
    private lateinit var game: Game

    companion object {
        var width: Int = 0
        var height: Int = 0
        lateinit var gl: GL
    }

    init {
        Companion.width = this.width
        Companion.height = this.height

        document.body?.onload = {
            val canvas = document.getElementById("game-canvas")!! as HTMLCanvasElement
            canvas.width = width
            canvas.height = height

            gl = canvas.getContext("webgl") as GL
            game = Game()

            initCallbacks()
            mainLoop(0.0)
        }
    }

    private fun initCallbacks() {
        val canvas = document.getElementById("game-canvas")!! as HTMLCanvasElement
        canvas.onwheel = {
            game.onWheel(Vec3(it.deltaX.toFloat(), it.deltaY.toFloat(), it.deltaZ.toFloat()))
        }

        canvas.onkeydown = {
            game.onKeyDown(it.key)
        }

        canvas.onkeyup = {
            game.onKeyUp(it.key)
        }
    }

    private fun mainLoop(currentTime: Double) {
        window.requestAnimationFrame { mainLoop(it) }

        val frameTime = currentTime - this.lastGameTime
        this.lastGameTime = frameTime

        game.updateAndRender()
    }
}