package game

import core.App
import gfx.BatchQuadRenderer
import gfx.Rectangle
import gfx.Texture
import gfx.TextureAtlas
import kotlinx.browser.window
import math.*
import org.khronos.webgl.WebGLRenderingContext
import kotlinx.coroutines.*

class Game {
    private val aspectRatio = App.width.toFloat() / App.height.toFloat()
    private var zoom = 100F
    private val batchRenderer = BatchQuadRenderer()
    private var cameraMatrix: Mat4 = Mat4.ortho3d(-aspectRatio * zoom, aspectRatio * zoom, -zoom, zoom, -1.0F, 1.0F)
    private var tilesAtlas = TextureAtlas()

    init {
        loadAssets()
    }

    private fun loadAssets() {
        CoroutineScope(window.asCoroutineDispatcher()).launch {
            tilesAtlas = TextureAtlas.load("atlas/tiles.json").await()
        }
    }

    fun updateAndRender() {

        App.gl.clearColor(0.0F, 0.0F, 0.0F, 1.0F)
        App.gl.clear(WebGLRenderingContext.COLOR_BUFFER_BIT)

        cameraMatrix = Mat4.ortho3d(-aspectRatio * zoom, aspectRatio * zoom, -zoom, zoom, -1.0F, 1.0F)

        batchRenderer.begin(cameraMatrix, tilesAtlas.getTexture())
        batchRenderer.drawSubTexture(Vec2(0F, 0F), Vec2(20F, 20F), tilesAtlas.getTileByName("hero"))
        batchRenderer.end()
    }

    fun onKeyDown(key: String) {

    }

    fun onKeyUp(key: String) {
    }

    fun onWheel(delta: Vec3) {
        zoom -= zoom / (0.1F * delta.y)
    }
}