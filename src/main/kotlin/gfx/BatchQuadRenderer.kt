package gfx

import core.App
import org.khronos.webgl.Float32Array
import org.khronos.webgl.Uint16Array
import org.khronos.webgl.WebGLBuffer
import org.khronos.webgl.WebGLRenderingContext as GL
import math.*

class BatchQuadRenderer {

    private lateinit var shader: Shader
    private val vertexBuffer: WebGLBuffer = App.gl.createBuffer()!!
    private val indexBuffer: WebGLBuffer = App.gl.createBuffer()!!
    private val vertices: MutableList<Float> = mutableListOf()

    private val maxQuads = 2000
    private val maxVertices = maxQuads * 4
    private val maxFloats = maxVertices * 5

    private lateinit var currentTexture: Texture

    init {
        initShader()
        initBuffers()
    }

    fun begin(cameraMatrix: Mat4, texture: Texture = Texture.WHITE_TEXTURE) {
        currentTexture = texture
        shader.bind()
        texture.bind()

        App.gl.uniformMatrix4fv(shader.uniformLocations["uProj"], false, cameraMatrix.toArray())
        App.gl.uniform1i(shader.uniformLocations["uTexture"], 0)
    }

    fun end() {
        flush()
    }

    fun draw(center: Vec2, halfDim: Vec2) {
        if (vertices.count() >= maxFloats) {
            flush()
        }

        vertices.addAll(arrayOf(
            center.x - halfDim.x, center.y + halfDim.y, 0F, 0F, 0F,
            center.x - halfDim.x, center.y - halfDim.y, 0F, 0F, 1F,
            center.x + halfDim.x, center.y - halfDim.y, 0F, 1F, 1F,
            center.x + halfDim.x, center.y + halfDim.y, 0F, 1F, 0F,
        ))
    }

    fun drawSubTexture(center: Vec2, halfDim: Vec2, sourceRectangle: Rectangle) {
        if (vertices.count() >= maxFloats) {
            flush()
        }

        val minU = sourceRectangle.x / currentTexture.width
        val maxV = sourceRectangle.y / currentTexture.height
        val maxU = minU + (sourceRectangle.w / currentTexture.width)
        val minV = maxV + (sourceRectangle.h / currentTexture.height)

        vertices.addAll(arrayOf(
            center.x - halfDim.x, center.y + halfDim.y, 0F, minU, maxV,
            center.x - halfDim.x, center.y - halfDim.y, 0F, minU, minV,
            center.x + halfDim.x, center.y - halfDim.y, 0F, maxU, minV,
            center.x + halfDim.x, center.y + halfDim.y, 0F, maxU, maxV,
        ))
    }

    private fun flush() {
        val numVerts = vertices.count() / 5
        val numQuads = numVerts / 4
        val numIndices = numQuads * 6

        App.gl.bindBuffer(GL.ARRAY_BUFFER, vertexBuffer)
        App.gl.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexBuffer)
        App.gl.bufferSubData(GL.ARRAY_BUFFER, 0, Float32Array(vertices.toTypedArray()))
        App.gl.drawElements(GL.TRIANGLES, numIndices, GL.UNSIGNED_SHORT, 0)
        vertices.clear()
    }

    private fun initShader() {
        val vertexShaderSource = """
        precision mediump float;
        attribute vec3 aPosition;
        attribute vec2 aTexCoord;
        uniform mat4 uProj;
        varying vec2 vTexCoord;
        void main() {
            vTexCoord = aTexCoord;
            gl_Position = uProj * vec4(aPosition, 1.0);
        }
        """.trimIndent()

        val fragmentShaderSource = """
        precision mediump float;
        uniform sampler2D uTexture;
        varying vec2 vTexCoord;

        void main() {
            gl_FragColor = vec4(texture2D(uTexture, vTexCoord).xyz, 1.0);
        }
        """.trimIndent()

        shader = Shader(vertexShaderSource, fragmentShaderSource)
    }

    private fun initBuffers() {
        App.gl.bindBuffer(GL.ARRAY_BUFFER, vertexBuffer)
        App.gl.bufferData(GL.ARRAY_BUFFER, maxVertices * Float.SIZE_BYTES, GL.DYNAMIC_DRAW)

        App.gl.enableVertexAttribArray(shader.attribLocations["aPosition"]!!)
        App.gl.enableVertexAttribArray(shader.attribLocations["aTexCoord"]!!)

        App.gl.vertexAttribPointer(shader.attribLocations["aPosition"]!!, 3, GL.FLOAT, false, 5 * Float.SIZE_BYTES, 0)
        App.gl.vertexAttribPointer(shader.attribLocations["aTexCoord"]!!, 2, GL.FLOAT, false, 5 * Float.SIZE_BYTES, 3 * Float.SIZE_BYTES)

        val indices = mutableListOf<Short>()

        for (i in 0 until maxQuads) {
            val offset: Short = (i*4).toShort()
            indices.add((0 + offset).toShort())
            indices.add((1 + offset).toShort())
            indices.add((2 + offset).toShort())
            indices.add((2 + offset).toShort())
            indices.add((3 + offset).toShort())
            indices.add((0 + offset).toShort())
        }

        App.gl.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexBuffer)
        App.gl.bufferData(GL.ELEMENT_ARRAY_BUFFER, Uint16Array(indices.toTypedArray()), GL.STATIC_DRAW)
    }

}