package gfx

class Color(
    var r: Float,
    var g: Float,
    var b: Float,
    var a: Float,
) {

    fun toArray(): Array<Float> {
        return arrayOf(r,g,b,a)
    }

    companion object {
        val WHITE = Color(1F, 1F, 1F,1F)
        val RED   = Color(1F, 0F, 0F,1F)
        val GREEN = Color(0F, 1F, 0F,1F)
        val BLUE  = Color(0F, 0F, 1F,1F)
    }
}