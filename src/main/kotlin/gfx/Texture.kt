package gfx

import core.App
import org.khronos.webgl.Uint8Array
import org.khronos.webgl.WebGLRenderingContext as GL
import org.w3c.dom.Image

class Texture {

    val textureId = App.gl.createTexture()!!
    var width = 0
    var height = 0

    fun bind() {
        App.gl.bindTexture(GL.TEXTURE_2D, textureId)
    }

    fun unbind() {
        App.gl.bindTexture(GL.TEXTURE_2D, null)
    }

    companion object {

        fun load(url: String): Texture {
            val image = Image()
            val result = loadWhiteTexture()

            image.onload = {
                result.width = image.width
                result.height = image.height
                App.gl.bindTexture(GL.TEXTURE_2D, result.textureId)
                App.gl.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, GL.RGBA, GL.UNSIGNED_BYTE, image)

                App.gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE)
                App.gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE)
                App.gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST)
                App.gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST)
            }

            image.src = url

            return result
        }

        private fun loadWhiteTexture(): Texture {
            val result = Texture()
            result.width = 1
            result.height = 1
            result.bind()
            val pixel = Uint8Array(arrayOf(255.toByte(), 255.toByte(), 255.toByte(), 255.toByte()))
            App.gl.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, 1, 1, 0, GL.RGBA, GL.UNSIGNED_BYTE, pixel)

            return result
        }

        val WHITE_TEXTURE = loadWhiteTexture()
    }
}