package gfx
import kotlinx.serialization.*

@Serializable
data class Rectangle(val x: Float = 0F, val y: Float = 0F, val w: Float = 0F, val h: Float = 0F)