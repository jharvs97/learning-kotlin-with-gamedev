package gfx

import kotlinx.browser.window
import kotlin.js.Promise

import kotlinx.serialization.*
import kotlinx.serialization.json.*

@Serializable
data class SubTextures(
    val path: String = "",
    val tiles: Map<String, Rectangle> = mapOf()
)

class TextureAtlas {
    private var atlasTexture: Texture = Texture.WHITE_TEXTURE
    private var subTextures = SubTextures()

    fun getTexture(): Texture = atlasTexture
    fun getTileByName(name: String): Rectangle {
        return subTextures.tiles[name] ?: Rectangle()
    }

    companion object {
        fun load(url: String): Promise<TextureAtlas> =
            Promise { resolve, reject ->
                window
                    .fetch(url)
                    .then {
                        it.text()
                    }.then {
                        val atlas = TextureAtlas()
                        try {
                            atlas.subTextures = Json.decodeFromString(it)
                            atlas.atlasTexture = Texture.load(atlas.subTextures.path)
                            resolve(atlas)
                        } catch (e: Exception) {
                            reject(e)
                        }
                    }
            }
    }

}