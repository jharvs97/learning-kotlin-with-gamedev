package gfx

import core.App
import org.khronos.webgl.WebGLProgram
import org.khronos.webgl.WebGLShader
import org.khronos.webgl.WebGLUniformLocation
import org.khronos.webgl.WebGLRenderingContext as GL

class Shader(vertexSource: String, fragmentSource: String) {
    var program: WebGLProgram = App.gl.createProgram()!!
    var attribLocations: Map<String, Int>
    var uniformLocations: Map<String, WebGLUniformLocation>

    init {
        val vertexShader = getVertexShader(vertexSource)
        val fragmentShader = getFragmentShader(fragmentSource)
        App.gl.attachShader(this.program, vertexShader)
        App.gl.attachShader(this.program, fragmentShader)
        App.gl.linkProgram(this.program)
        if(!(App.gl.getProgramParameter(this.program, GL.LINK_STATUS) as Boolean)) {
            println(App.gl.getProgramInfoLog(this.program))
        }
        App.gl.deleteShader(vertexShader)
        App.gl.deleteShader(fragmentShader)
        this.attribLocations = getShaderAttributeLocations()
        this.uniformLocations = getShaderUniformLocations()
    }

    fun bind() {
        App.gl.useProgram(this.program)
    }

    fun unbind() {
        App.gl.useProgram(null)
    }

    private fun getFragmentShader(source: String): WebGLShader = getShader(source, GL.FRAGMENT_SHADER)
    private fun getVertexShader(source: String): WebGLShader = getShader(source, GL.VERTEX_SHADER)

    private fun getShader(shaderSource: String, shaderType: Int): WebGLShader {
        val shader = App.gl.createShader(shaderType)
        App.gl.shaderSource(shader, shaderSource)
        App.gl.compileShader(shader)
        if(!(App.gl.getShaderParameter(shader, GL.COMPILE_STATUS) as Boolean)) {
            println(App.gl.getShaderInfoLog(shader))
        }
        return shader ?: throw IllegalStateException("Failed to create shader")
    }

    private fun getShaderAttributeLocations(): Map<String, Int> {
        App.gl.useProgram(this.program)

        val attribLocations: MutableMap<String, Int> = mutableMapOf()
        val activeAttributes = App.gl.getProgramParameter(this.program, GL.ACTIVE_ATTRIBUTES) as Int

        for (i in 0 until activeAttributes) {
            val attribInfo = App.gl.getActiveAttrib(this.program, i) ?: throw IllegalStateException("Failed to get active attribute at index $i")
            val attribLoc = App.gl.getAttribLocation(this.program, attribInfo.name)

            attribLocations[attribInfo.name] = attribLoc
        }

        App.gl.useProgram(null)

        return attribLocations.toMap()
    }

    private fun getShaderUniformLocations(): Map<String, WebGLUniformLocation> {
        App.gl.useProgram(this.program)

        val uniformLocations: MutableMap<String, WebGLUniformLocation> = mutableMapOf()
        val activeUniforms = App.gl.getProgramParameter(this.program, GL.ACTIVE_UNIFORMS) as Int

        for (i in 0 until activeUniforms) {
            val uniformInfo = App.gl.getActiveUniform(this.program, i) ?: throw IllegalStateException("Failed to get active uniform at index $i")
            val uniformLoc = App.gl.getUniformLocation(this.program, uniformInfo.name) ?: throw IllegalStateException("Failed to get uniform location for ${uniformInfo.name}")
            uniformLocations[uniformInfo.name] = uniformLoc
        }

        App.gl.useProgram(null)

        return uniformLocations.toMap()
    }
}