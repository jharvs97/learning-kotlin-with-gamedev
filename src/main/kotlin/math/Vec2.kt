package math

data class Vec2(
    var x: Float = 0F,
    var y: Float = 0F
) {
    constructor(array: Array<Float>) : this(array[0],array[1])

    fun toArray(): Array<Float> {
        return arrayOf(x, y)
    }
}
