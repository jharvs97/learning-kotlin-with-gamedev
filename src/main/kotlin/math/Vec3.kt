package math

data class Vec3(
    var x: Float = 0F,
    var y: Float = 0F,
    var z: Float = 0F,
) {
    constructor(array: Array<Float>) : this(array[0],array[1],array[2])

    fun xy() = Vec2(x,y)

    fun toArray(): Array<Float> {
        return arrayOf(x,y,z)
    }

}