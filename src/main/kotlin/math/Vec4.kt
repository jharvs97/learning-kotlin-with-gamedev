package math

data class Vec4(
    var x: Float = 0F,
    var y: Float = 0F,
    var z: Float = 0F,
    var w: Float = 0F
) {
    constructor(array: Array<Float>) : this(array[0],array[1],array[2],array[3])
    constructor(other: Vec4) : this(other.x, other.y, other.z, other.w)

    fun xyz() = Vec3(x,y,z)
    fun xy()  = Vec2(x,y)

    fun dot(other: Vec4): Float {
        return (this.x * other.x) + (this.y * other.y) + (this.z * other.z) + (this.w * other.w)
    }

    fun toArray(): Array<Float> {
        return arrayOf(x,y,z,w)
    }
}