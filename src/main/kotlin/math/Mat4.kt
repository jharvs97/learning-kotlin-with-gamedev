package math

typealias Mat4Type = Array<Array<Float>>

data class Mat4(
    var mat: Mat4Type = Array(4) { Array(4) { 0F } }
) {
    constructor(array: Array<Float>) : this() {
        if (array.size < 16 || array.size > 16) {
            throw Exception("Array must have 16 elements!")
        }

        for (i in 0 until 4) {
            for (j in 0 until 4) {
                this.mat[i][j] = array[4 * i + j]
            }
        }
    }

    constructor(other: Mat4) : this() {
        this.mat = other.mat.copyOf()
    }

    fun toArray(): Array<Float> {
        return this.mat.copyOf().flatMap { it.asIterable() }.toTypedArray()
    }

    fun getColumns(): Array<Vec4> {
        return this.mat.copyOf().map { Vec4(it) }.toTypedArray()
    }

    fun getRows(): Array<Vec4> {
        val rows = Mat4()
        for (i in 0 until 4) {
            for (j in 0 until 4) {
                rows[j][i] = this.mat[i][j]
            }
        }
        return rows.mat.map { Vec4(it) }.toTypedArray()
    }

    operator fun get(index: Int): Array<Float> {
        return this.mat[index]
    }

    operator fun times(other: Mat4): Mat4 {
        val rows = this.getRows()
        val cols = other.getColumns()
        val result = Mat4()

        for (i in 0 until 4) {
            for (j in 0 until 4) {
                result[i][j] = rows[j].dot(cols[i])
            }
        }

        return result
    }

    companion object {

        fun identity(): Mat4 {
            val mat4 = Mat4()
            mat4[0][0] = 1F
            mat4[1][1] = 1F
            mat4[2][2] = 1F
            mat4[3][3] = 1F
            return mat4
        }

        fun translate(v: Vec3): Mat4 {
            val mat4 = identity()
            mat4[3][0] = v.x
            mat4[3][1] = v.y
            mat4[3][2] = v.z
            return mat4
        }

        fun translate(x: Float, y: Float, z: Float): Mat4 {
            return translate(Vec3(x,y,z))
        }

        fun scale(v: Vec3): Mat4 {
            val mat4 = identity()
            mat4[0][0] = v.x
            mat4[1][1] = v.x
            mat4[2][2] = v.x
            return mat4
        }

        fun scale(x: Float, y: Float, z: Float): Mat4 {
            return scale(Vec3(x,y,z))
        }

        fun scale(a: Float): Mat4 {
            return scale(Vec3(a,a,a))
        }

        fun ortho3d(left: Float, right: Float, bottom: Float, top: Float, zNear: Float, zFar: Float): Mat4 {
            val result = identity()

            result[0][0] = 2F / (right - left)
            result[1][1] = 2F / (top - bottom)
            result[2][2] = (-2F) / (zFar - zNear)
            result[3][0] = -((right + left) / (right - left))
            result[3][1] = -((top + bottom) / (top - bottom))
            result[3][2] = -((zFar + zNear) / (zFar - zNear))

            return result
        }
    }
}